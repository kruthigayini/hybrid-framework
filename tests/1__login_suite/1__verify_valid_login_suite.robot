*** Settings ***
Documentation   This suite verifies if valid users are able to login to the dashboard
...    nd connected to test case TC_OH_2

Resource    ../../resource/base/CommonFunctionalities.resource

Test Setup    Launch Browser and Navigate to Url
Test Teardown   Close Browser

*** Test Cases ***
TC_OH_02_Verify Valid Login
    Input Text    name=username    admin
    Input Password    name=password    admin123
    Click Button    xpath=//button[normalize-space()='Login']
    Element Text Should Be    xpath=//h6[normalize-space()='Dashboard']    Dashboard

Print_title
    Log Title

NonBrowser_Testcase
    [Setup]     None
    Log To Console    No browser
    [Teardown]      Log To Console    None to log to output


