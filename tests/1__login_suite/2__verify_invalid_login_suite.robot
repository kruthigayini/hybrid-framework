*** Settings ***
Documentation   This suite verifies if valid users are able to login to the dashboard
...    nd connected to test case TC_OH_2

Resource    ../../resource/base/CommonFunctionalities.resource

Test Setup    Launch Browser and Navigate to Url
Test Teardown   Close Browser

*** Test Cases ***
Verify Invalid Login
    Input Text    name=username    john
    Input Password    name=password    johan
    Click Button    xpath=//button[normalize-space()='Login']
    Element Text Should Be    xpath=//p[contains(normalize-space(),'Invalid')]   Invalid credentials
